# JouerFlux

JouerFlux est une application Flask permettant de gérer des firewalls, des politiques de filtrage et des règles.

## Installation

1. Clonez le dépôt
2. Installez les dépendances
    ```bash
    pip install -r requirements.txt
    ```
3. Initialisez la base de données
    ```bash
    flask db init
    flask db migrate
    flask db upgrade
    ```
4. Lancez l'application
    ```bash
    python app.py
    ```

## Utilisation

L'application expose des endpoints pour gérer les firewalls, les politiques de filtrage et les règles. Vous pouvez accéder à la documentation Swagger à `http://localhost:5000/swagger`.

## Docker

Pour exécuter l'application avec Docker :
1. Construisez l'image Docker
    ```bash
    docker build -t jouerflux .
    ```
2. Lancez le conteneur
    ```bash
    docker run -p 5000:5000 jouerflux
    ```

Accédez ensuite à l'application à `http://localhost:5000/swagger.
