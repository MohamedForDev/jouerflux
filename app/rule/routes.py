from flask import Blueprint, request, jsonify
from app.models import db, Rule ,Policy
from app.utils import serialize_rule

rule_bp = Blueprint('rule_bp', __name__)

@rule_bp.route('/policies/<int:policy_id>/rules', methods=['POST'])
def add_rule(policy_id):
    data = request.get_json()
    name = data.get('name')
    action = data.get('action')
    source = data.get('source')
    destination = data.get('destination')
    protocol = data.get('protocol')
    policy = Policy.query.get(policy_id)
    if not policy:
        return jsonify({"message": "Policy not found"}), 404
    new_rule = Rule(name=name, action=action,source=source,destination=destination,protocol=protocol,policy_id=policy_id)
    db.session.add(new_rule)
    db.session.commit()
    return jsonify({'id': new_rule.id, 'message': "Rule Added"}), 201

@rule_bp.route('/policies/<int:policy_id>/rules', methods=['GET'])
def get_rules(policy_id):
    policy = Policy.query.get(policy_id)
    if not policy:
        return jsonify({"message": "Policy not found"}), 404
    rules = [serialize_rule(rule) for rule in policy.rules]

    return jsonify(rules), 200

@rule_bp.route('/rules/<int:rule_id>', methods=['DELETE'])
def delete_rule(rule_id):
    rule = Rule.query.get(rule_id)
    if not rule:
        return jsonify({"message": "Rule not found"}), 404
    db.session.delete(rule)
    db.session.commit()
    return jsonify({"message": "Rule deleted"}), 200

