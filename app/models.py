from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Firewall(db.Model):
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    name = db.Column(db.String(20), nullable=False)
    description = db.Column(db.String(100), nullable=False)
    policies = db.relationship('Policy', backref='firewall', lazy=True)

class Policy(db.Model):
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    name = db.Column(db.String(80), nullable=False)
    description = db.Column(db.String(100), nullable=False)
    action= db.Column(db.String(100), nullable=False)
    firewall_id = db.Column(db.Integer, db.ForeignKey('firewall.id'), nullable=True)
    rules = db.relationship('Rule', backref='policy', lazy=True)

class Rule(db.Model):
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    name = db.Column(db.String(80), nullable=False)
    action = db.Column(db.String(80), nullable=False)
    source = db.Column(db.String(80), nullable=False)
    destination = db.Column(db.String(80), nullable=False)
    protocol = db.Column(db.String(80), nullable=False)
    policy_id = db.Column(db.Integer, db.ForeignKey('policy.id'), nullable=True)