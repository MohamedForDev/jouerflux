from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config

db = SQLAlchemy()
migrate = Migrate()

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)

    from app.firewall import firewall_bp as firewalls_bp
    app.register_blueprint(firewalls_bp, url_prefix='/api/firewall')

    from app.policy import policy_bp as policies_bp
    app.register_blueprint(policies_bp, url_prefix='/api/policie')

    from app.rule import rule_bp as rules_bp
    app.register_blueprint(rules_bp, url_prefix='/api/rule')

    return app
