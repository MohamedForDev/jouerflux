from flask import Blueprint, request, jsonify
from app.models import db, Policy ,Firewall
from app.utils import serialize_policy
policy_bp = Blueprint('policy_bp', __name__)

@policy_bp.route('/firewalls/<int:firewall_id>/policies', methods=['GET'])
def get_policies(firewall_id):
    try:
        firewall = Firewall.query.get(firewall_id)
        if not firewall:
            return jsonify({'error': 'Firewall not found'}), 404
        policies = [serialize_policy(policy) for policy in firewall.policies]

        return jsonify(policies), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500

@policy_bp.route('/firewalls/<int:firewall_id>/policies', methods=['POST'])
def add_policy(firewall_id):
    try:
        firewall = Firewall.query.get(firewall_id)
        if not firewall:
            return jsonify({'error': 'Firewall not found'}), 404
        data = request.get_json()
        name = data.get('name')
        description = data.get('description')
        action = data.get('action')
        new_policy = Policy( name=name, firewall_id=firewall.id,description=description,action=action)
        db.session.add(new_policy)
        db.session.commit()

        return jsonify({'id': new_policy.id, "message": "Policy added"}), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 500

@policy_bp.route('/policies/<int:policy_id>', methods=['DELETE'])
def delete_policy(policy_id):
    try:
        policy = Policy.query.get(policy_id)
        if not policy:
            return jsonify({'error': 'Policy not found'}), 404

        db.session.delete(policy)
        db.session.commit()

        return jsonify({'message': 'Policy deleted successfully'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500




