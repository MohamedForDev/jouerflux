
def serialize_rule(rule):
    return {
        'id': rule.id,
        'action': rule.action,
        'source': rule.source,
        'destination': rule.destination,
        'protocol': rule.protocol,
        # Add any other attributes of the Rule model that need to be serialized
    }

def serialize_policy(policy):
    return {
        'id': policy.id,
        'rules': [serialize_rule(rule) for rule in policy.rules],
        'name': policy.name,
        'description': policy.description
    }