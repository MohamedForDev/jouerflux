from flask import Blueprint, request, jsonify
from app.models import db, Firewall
from app.utils import serialize_rule

firewall_bp = Blueprint('firewall_bp', __name__)

@firewall_bp.route('/firewalls', methods=['POST'])
def add_firewall():
    data = request.get_json()
    new_firewall = Firewall(name=data['name'],description=data['description'])
    db.session.add(new_firewall)
    db.session.commit()
    return jsonify({"message": "Firewall added","id":new_firewall.id}), 201

@firewall_bp.route('/firewalls', methods=['GET'])
def get_firewalls():
    try:
        firewalls = Firewall.query.all()
        firewall_list = [{'id': fw.id,
                          'name': fw.name ,
                          'description':fw.description ,
                          'policies': [{'id': policy.id, 'rules': [serialize_rule(rule) for rule in policy.rules], 'name': policy.name ,'description':policy.description} for policy in fw.policies]} for fw in firewalls]
        return jsonify(firewall_list), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500

@firewall_bp.route('/firewalls/<int:id>', methods=['GET'])
def get_firewall(id):
    firewall = Firewall.query.get(id)
    print(firewall)
    if firewall:
        return jsonify({'id': firewall.id,
                        'name': firewall.name ,
                        'description':firewall.description,
                        'policies': [{'id': policy.id,'name':policy.name, 'rules': [serialize_rule(rule) for rule in policy.rules], 'description': policy.description} for policy in firewall.policies]

                        } )
    else:
        return jsonify({"message": "Firewall not found"}), 404

@firewall_bp.route('/firewalls/<int:id>', methods=['DELETE'])
def delete_firewall(id):
    firewall = Firewall.query.get(id)
    if firewall:
        db.session.delete(firewall)
        db.session.commit()
        return jsonify({"message": "Firewall deleted"}), 200
    return jsonify({"message": "Firewall not found"}), 404