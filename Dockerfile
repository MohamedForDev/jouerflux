FROM python:3.9-slim-buster

# Set the working directory in the container
WORKDIR /app

# Copy only the requirements file to leverage Docker cache
COPY requirements.txt .

# Install the dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application code
COPY . .

# Set an environment variable to ensure the application runs in production mode
ENV FLASK_ENV=production

# Expose port 5000 for the Flask app
EXPOSE 5000

# Command to run the application
CMD ["python", "./app.py"]
