{
  "swagger": "2.0",
  "info": {
    "description": "This is the API documentation for JouerFlux.",
    "version": "1.0.0",
    "title": "JouerFlux API"
  },
  "host": "127.0.0.1:5000",
  "basePath": "/api",
  "tags": [
    {
      "name": "firewall",
      "description": "Operations related to firewalls"
    },
    {
      "name": "policy",
      "description": "Operations related to policies"
    },
    {
      "name": "rule",
      "description": "Operations related to rules"
    }
  ],
  "paths": {
    "/firewalls": {
      "get": {
        "tags": ["firewall"],
        "summary": "Get all firewalls",
        "responses": {
          "200": {
            "description": "A list of firewalls",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Firewall"
              }
            }
          }
        }
      },
      "post": {
        "tags": ["firewall"],
        "summary": "Create a new firewall",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Firewall object to be created",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Firewall"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Firewall created"
          }
        }
      }
    },
    "/firewalls/{firewallId}": {
      "get": {
        "tags": ["firewall"],
        "summary": "Get a firewall by ID",
        "parameters": [
          {
            "name": "firewallId",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "A firewall object",
            "schema": {
              "$ref": "#/definitions/Firewall"
            }
          }
        }
      },
      "delete": {
        "tags": ["firewall"],
        "summary": "Delete a firewall by ID",
        "parameters": [
          {
            "name": "firewallId",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "204": {
            "description": "Firewall deleted"
          }
        }
      }
    },
    "/firewalls/{firewallId}/policies": {
      "get": {
        "tags": ["policy"],
        "summary": "Get all policies for a specific firewall",
        "parameters": [
          {
            "name": "firewallId",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "A list of policies",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Policy"
              }
            }
          },
          "404": {
            "description": "Firewall not found"
          },
          "500": {
            "description": "Internal server error"
          }
        }
      },
      "post": {
        "tags": ["policy"],
        "summary": "Add a new policy to a specific firewall",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Policy object to be created",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Policy"
            }
          },
          {
            "name": "firewallId",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "201": {
            "description": "Policy created"
          },
          "404": {
            "description": "Firewall not found"
          },
          "500": {
            "description": "Internal server error"
          }
        }
      }
    },
    "/policies/{policyId}": {
      "delete": {
        "tags": ["policy"],
        "summary": "Delete a policy by ID",
        "parameters": [
          {
            "name": "policyId",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "Policy deleted successfully"
          },
          "404": {
            "description": "Policy not found"
          },
          "500": {
            "description": "Internal server error"
          }
        }
      }
    },
    "/policies/{policyId}/rules": {
      "post": {
        "tags": ["rule"],
        "summary": "Add a new rule to a specific policy",
        "parameters": [
          {
            "name": "policyId",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int64"
          },
          {
            "in": "body",
            "name": "body",
            "description": "Rule object to be created",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Rule"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Rule created"
          },
          "404": {
            "description": "Policy not found"
          },
          "500": {
            "description": "Internal server error"
          }
        }
      },
      "get": {
        "tags": ["rule"],
        "summary": "Get all rules for a specific policy",
        "parameters": [
          {
            "name": "policyId",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "A list of rules",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Rule"
              }
            }
          },
          "404": {
            "description": "Policy not found"
          },
          "500": {
            "description": "Internal server error"
          }
        }
      }
    },
    "/rules/{ruleId}": {
      "delete": {
        "tags": ["rule"],
        "summary": "Delete a rule by ID",
        "parameters": [
          {
            "name": "ruleId",
            "in": "path",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "Rule deleted successfully"
          },
          "404": {
            "description": "Rule not found"
          },
          "500": {
            "description": "Internal server error"
          }
        }
      }
    }
  },
  "definitions": {
    "Firewall": {
      "type": "object",
      "required": ["name"],
      "properties": {
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        }
      }
    },
    "Policy": {
      "type": "object",
      "required": ["name"],
      "properties": {
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
         "action": {
          "type": "string"
        }
      }
    },
    "Rule": {
      "type": "object",
      "required": ["name", "action"],
      "properties": {
        "name": {
          "type": "string"
        },
        "action": {
          "type": "string"
        },
        "source": {
          "type": "string"
        },
        "destination": {
          "type": "string"
        },
        "protocol": {
          "type": "string"
        }
      }
    }
  }
}
