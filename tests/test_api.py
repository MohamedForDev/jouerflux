def test_add_firewall(client):
    response = client.post('/api/firewalls', json={
        'name': 'Test Firewall'
    })
    assert response.status_code == 201
    data = response.get_json()
    assert 'id' in data

def test_get_firewalls(client):
    client.post('/api/firewalls', json={
        'name': 'Test Firewall'
    })
    response = client.get('/api/firewalls')
    assert response.status_code == 200
    data = response.get_json()
    assert len(data) == 1
    assert data[0]['name'] == 'Test Firewall'

def test_delete_firewall(client):
    response = client.post('/api/firewalls', json={
        'name': 'Test Firewall'
    })
    firewall_id = response.get_json()['id']

    response = client.delete(f'/api/firewalls/{firewall_id}')
    assert response.status_code == 204

def test_add_policy(client):
    response = client.post('/api/firewalls', json={
        'name': 'Test Firewall'
    })
    firewall_id = response.get_json()['id']

    response = client.post(f'/api/firewalls/{firewall_id}/policies', json={
        'name': 'Test Policy'
    })
    assert response.status_code == 201
    data = response.get_json()
    assert 'id' in data

def test_get_policies(client):
    response = client.post('/api/firewalls', json={
        'name': 'Test Firewall'
    })
    firewall_id = response.get_json()['id']

    client.post(f'/api/firewalls/{firewall_id}/policies', json={
        'name': 'Test Policy'
    })
    response = client.get('/api/policies')
    assert response.status_code == 200
    data = response.get_json()
    assert len(data) == 1
    assert data[0]['name'] == 'Test Policy'

def test_delete_policy(client):
    response = client.post('/api/firewalls', json={
        'name': 'Test Firewall'
    })
    firewall_id = response.get_json()['id']

    response = client.post(f'/api/firewalls/{firewall_id}/policies', json={
        'name': 'Test Policy'
    })
    policy_id = response.get_json()['id']

    response = client.delete(f'/api/policies/{policy_id}')
    assert response.status_code == 204

def test_add_rule(client):
    response = client.post('/api/firewalls', json={
        'name': 'Test Firewall'
    })
    firewall_id = response.get_json()['id']

    response = client.post(f'/api/firewalls/{firewall_id}/policies', json={
        'name': 'Test Policy'
    })
    policy_id = response.get_json()['id']

    response = client.post(f'/api/policies/{policy_id}/rules', json={
        'name': 'Test Rule',
        'action': 'allow'
    })
    assert response.status_code == 201
    data = response.get_json()
    assert 'id' in data

def test_get_rules(client):
    response = client.post('/api/firewalls', json={
        'name': 'Test Firewall'
    })
    firewall_id = response.get_json()['id']

    response = client.post(f'/api/firewalls/{firewall_id}/policies', json={
        'name': 'Test Policy'
    })
    policy_id = response.get_json()['id']

    client.post(f'/api/policies/{policy_id}/rules', json={
        'name': 'Test Rule',
        'action': 'allow'
    })
    response = client.get('/api/rules')
    assert response.status_code == 200
    data = response.get_json()
    assert len(data) == 1
    assert data[0]['name'] == 'Test Rule'
    assert data[0]['action'] == 'allow'

def test_delete_rule(client):
    response = client.post('/api/firewalls', json={
        'name': 'Test Firewall'
    })
    firewall_id = response.get_json()['id']

    response = client.post(f'/api/firewalls/{firewall_id}/policies', json={
        'name': 'Test Policy'
    })
    policy_id = response.get_json()['id']

    response = client.post(f'/api/policies/{policy_id}/rules', json={
        'name': 'Test Rule',
        'action': 'allow'
    })
    rule_id = response.get_json()['id']

    response = client.delete(f'/api/rules/{rule_id}')
    assert response.status_code == 204
