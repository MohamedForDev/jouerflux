from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint
from app.models import db
from app.firewall.routes import firewall_bp
from app.policy.routes import policy_bp
from app.rule.routes import rule_bp
from flask_cors import CORS


def create_app():
    app = Flask(__name__)
    CORS(app)
    app.config.from_object('config.Config')
    db.init_app(app)

    with app.app_context():
        db.create_all()

    app.register_blueprint(firewall_bp, url_prefix='/api')
    app.register_blueprint(policy_bp, url_prefix='/api')
    app.register_blueprint(rule_bp, url_prefix='/api')

    SWAGGER_URL = '/swagger'
    API_URL = '/static/swagger.json'
    swaggerui_blueprint = get_swaggerui_blueprint(
        SWAGGER_URL, API_URL,
        config={'app_name': "JouerFlux"}
    )
    app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)

    return app


if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=5000,debug=True)
